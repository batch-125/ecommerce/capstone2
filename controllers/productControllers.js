const Product = require('./../models/Product');


// add a product
module.exports.addProduct = (reqBody) => {
    // console.log("reqBody from add Product",reqBody)

    let newProduct = new Product({
        image: reqBody.image,
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    })

    //model.method SAVE
    return newProduct.save().then((result, error)=>{
        if(error){
            return error;
        } else {
            return true;
        }
    })
}

// retrieve all products
module.exports.getAllProducts = () => {

    return Product.find().then(result => {
        return result
    })
}

// retrieve all ACTIVE products
module.exports.activeProducts = () => {

    return Product.find({ isActive: true}).then(result => {
        return result;
    })
}

// get single product with :productId
module.exports.getSingleProduct = (params) => {

    return Product.findById(params).then(result => {
        return result;
    })
}

// Update product
module.exports.updateProduct = (params, reqBody) => {

    let updatedProduct = {
        image: reqBody.image,
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    // Model.method is .findByIdAndUpdate(productId, wheretogetnewinfo, showupdated)
    return Product.findByIdAndUpdate(params, updatedProduct, {new:true})
    .then((result, error)=> {
        if(error){
            return error;
        } else {
            return result;
        }
    })
}

// Archive Product
module.exports.archiveProduct = (params) => {

    let updatedIsActiveProduct = {
        isActive: false
    }

    return Product.findByIdAndUpdate(params, updatedIsActiveProduct, {new:true})
    .then((result, error)=>{
        if(error){
            return error;
        } else {
            return result;
        }
    })
}

// Unrchive Product
module.exports.unarchiveProduct = (params) => {

    let updatedIsActiveProduct = {
        isActive: true
    }

    return Product.findByIdAndUpdate(params, updatedIsActiveProduct, {new:true})
    .then((result, error)=>{
        if(error){
            return error;
        } else {
            return result;
        }
    })
}

// Delete Product
module.exports.deleteProduct = (params)=> {

    return Product.findByIdAndDelete(params)
    .then((result,error)=>{
        if(error){
            return error;
        } else {
            return true;
        }
    })
}