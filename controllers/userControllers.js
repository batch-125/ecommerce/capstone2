const User = require("./../models/User");
const bcrypt = require("bcrypt");
const auth = require("./../auth");
const express = require("express");



module.exports.register = (reqBody) => {

    let newUser = new User ({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then( (result, error) => {
        if(error){
            return error;
        } else {
            return true;
        }
    })
}

module.exports.checkEmailExists = (reqBody) => {
    // console.log(reqBody)
    // model.method
    return User.find({ email: reqBody.email})
    // console.log(result)
    .then((result) => {
        if(result.length != 0){
            return true;
        } else {
            return false;
        }
    })
}

module.exports.login = (reqBody) => {
    // model.method
    return User.findOne({email: reqBody.email})
    .then(result => {
        console.log("userInfo from login",result)
        if(result == null){
            return false; 
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 
            
            if (isPasswordCorrect === true){
                return { access: auth.createAccessToken(result.toObject())}
            } else {
                return false;
            }
        }
    })
}

module.exports.getProfile = (data) => {
    console.log("data from getProfile", data)
    // Model.method
    return User.findById(data).then(result => {
        
        result.password = "*******"
        return result
    })
}

module.exports.setAdmin = (params) => {
    let isNowAdmin = {
        isAdmin: true
    }
    console.log(params)
    return User.findByIdAndUpdate(params, isNowAdmin, {new:true})
    .then((result, error)=>{
        if(error){
            return false;
        } else {
            return true
        }
    })
}

module.exports.unsetAdmin = (params) => {
    let isNotAdmin = {
        isAdmin: false
    }
    console.log(params)
    return User.findByIdAndUpdate(params, isNotAdmin, {new:true})
    .then((result, error)=>{
        if(error){
            return false;
        } else {
            return true;
        }
    })
}

module.exports.deleteUser = (params) => {

    return User.findByIdAndDelete(params)
    .then((result,error)=>{
        if(error){
            return error;
        } else {
            return true;
        }
    })
}

