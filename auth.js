// will be used in 
    // login
    // retrieve user details

// 1. require JWT
let jwt = require("jsonwebtoken");
let secret = "Ecommerce!";

// 2. create token
module.exports.createAccessToken = (user) => {

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
        firstName: user.firstName
    }
    // console.log(data)
    // console.log( jwt.sign(data, secret, {}))
    return jwt.sign(data, secret, {})
   
}

// 3. verify token
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization
    // console.log("token from verify",token)
    
    if(typeof token !== "undefined"){
        token = token.slice(7, token.length);
    
        return jwt.verify(token, secret, (error, data)=>{
            if(error){
                return res.send({auth:"failed"})
            } else {
                next();
            }
        })
    }
}   

// 4. decode token
module.exports.decode = (token) => {
    // console.log("token from decode", token)

    if(typeof token !== "undefined"){
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (error, data)=>{
            if(error){
                return null
            } else {
                return jwt.decode(token, {complete:true}).payload;
            }  
        })
    }
}