// 1. require express
const express = require("express");
const router = express.Router();

// controllers
const userController = require("./../controllers/userControllers");
const orderController = require('./../controllers/orderControllers');
const auth = require("./../auth");

// route to first verify email
router.post("/checkEmail", (req, res)=>{
    // console.log(req.body)
    userController.checkEmailExists(req.body).then(result => res.send(result));
});

//register when email verification done
router.post("/register", (req, res)=> {
    userController.register(req.body).then(result => res.send(result));
})

// login after done registering
router.post("/login", (req,res)=> {
    userController.login(req.body).then(result => res.send(result));
})

// details
router.get("/details", auth.verify, (req, res)=>{

    const userData = auth.decode(req.headers.authorization)
    console.log("userData from details", userData)

    userController.getProfile(userData.id).then(result => res.send(result))
})

// Make user an admin
router.put("/:userId/setAsAdmin", auth.verify, (req,res)=>{
    console.log("params from setAsAdmin", req.params.userId)
    userController.setAdmin(req.params.userId).then(result => res.send(result))
})

// Remove Admin: true from user
router.put("/:userId/removeAdmin", auth.verify, (req,res)=>{
    console.log("params from setAsAdmin", req.params.userId)
    userController.unsetAdmin(req.params.userId).then(result => res.send(result))
})

// Delete User 
router.delete("/:userId/delete", auth.verify, (req,res)=>{

    userController.deleteUser(req.params.userId).then(result => res.send(result))
})




// -----------------------------------------------------------------------------//


// Create order
// router.post("/add-to-cart", auth.verify, (req,res) => {
    
//         let data = {
//             userId: auth.decode(req.headers.authorization).id,
//             productId: req.body.productId,
//             totalPrice: req.body.price,
//             quantity: req.body.quantity
//         }
//     orderController.createOrder(data).then( result => res.send(result))
// })

// router.post("/checkout", auth.verify, (req, res) => {

// })


// // Retrieve User Orders
// router.get("/orders", auth.verify, (req,res)=> {
    
//     orderController.retrieveOrder().then(result => res.send(result))
// })


module.exports = router; 