// 1. require express
const express = require("express");
const router = express.Router();
const User = require("./../models/User");
const Product = require("./../models/Product");


// controllers
const orderControllers = require("./../controllers/orderControllers")
const auth = require("./../auth")



// Create Cart
router.post("/addCart", auth.verify, (req,res) => {
    let data = auth.decode(req.headers.authorization);
    // console.log(data)

    orderControllers.addCart(data).then(result => res.send(result))
})


router.post("/:productId/add-to-cart", auth.verify, (req,res) => {
    let data = auth.decode(req.headers.authorization);
    // const prod = Product.find({id: req.params.productId})
    // return prod;

    orderControllers.createOrder(req.params.productId, data, req.body).then(result => res.send(result))
})

router.get("/show-cart", auth.verify, (req,res)=> {
    let data = auth.decode(req.headers.authorization);

    orderControllers.retrieveOrder(data).then(result => res.send(result));
})

router.get("/all", auth.verify, (req,res)=> {
    let data = auth.decode(req.headers.authorization);

    orderControllers.allOrders(data).then(result => res.send(result));
})

router.delete("/:productId/remove-from-cart", auth.verify, (req,res) => {
    let data = auth.decode(req.headers.authorization);

    orderControllers.removeOrder(req.params.productId, data).then(result => res.send(result))
})

module.exports = router; 