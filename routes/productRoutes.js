// 1. require express
const express = require("express");
const router = express.Router();


// controllers
const productController = require("./../controllers/productControllers")
const auth = require("./../auth")


// add product/s
router.post("/add", auth.verify, (req,res) => {
    // console.log ("req.body from add Product", req.body)

    productController.addProduct(req.body).then(result => res.send(result))
})


// retrieve all products
router.get("/all", auth.verify, (req,res)=> {

    productController.getAllProducts().then(result => res.send(result));
})


// retrieve all active products
router.get("/active", (req, res)=>{
    
    productController.activeProducts().then(result => res.send(result));
})


// Get Single Product 
router.get("/:productId", (req,res)=>{
    // I put req.body here to make my router.put(update) of same url work.
    productController.getSingleProduct(req.params.productId, req.body).then(result => res.send(result));
})


// Update product
router.put("/:productId/edit", auth.verify, (req,res)=>{
    
    productController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
})

// Archive a product
router.put("/:productId/archive", (req,res)=> {

    productController.archiveProduct(req.params.productId).then(result => res.send(result));
})

// Unarchive a product
router.put("/:productId/unarchive", (req,res)=> {

    productController.unarchiveProduct(req.params.productId).then(result => res.send(result));
})

// Delete Product 
router.delete("/:productId/delete", (req,res)=>{

    productController.deleteProduct(req.params.productId).then(result => res.send(result))
})

module.exports = router;